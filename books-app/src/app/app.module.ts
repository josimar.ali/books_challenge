import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BooksComponent } from './books/books.component';
import { Routes, RouterModule }  from '@angular/router'
import { HttpClientModule } from '@angular/common/http';
import { BooksService } from './books/books.service';
import { FormComponent } from './books/form.component';
import { FormsModule } from '@angular/forms';
import { DigitOnlyModule } from '@uiowa/digit-only';

const routes: Routes = [
  { path: '', redirectTo: '/books', pathMatch: 'full' },
  { path: 'books', component: BooksComponent },
  { path: 'books/form', component: FormComponent },
  { path: 'books/form/:id', component: FormComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BooksComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    DigitOnlyModule
  ],
  providers: [BooksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
