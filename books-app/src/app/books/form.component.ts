import { Component, OnInit } from '@angular/core';
import { Book } from './book';
import { BooksService } from './books.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {
  book: Book = new Book();
  title: string = 'Add new book';

  constructor(private bookService: BooksService, private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadBook();
  }

  addBook() {
    this.bookService.addBook(this.book).subscribe(
      response => {
        this.router.navigate(['/books']);
        this.showInfo('Book added', `${this.book.name} added!`);
      },
      error => console.log(error)
    );
  }

  loadBook(): void {
    this.activatedRoute.params.subscribe(
      params => {
        let id = params['id'];
        if(id) {
          this.title = 'Edit book';
          this.bookService.getBook(id).subscribe(book => this.book = book);
        }
      }
    );
  }

  updateBook(): void {
    this.bookService.updateBook(this.book).subscribe(
      book => {
        this.router.navigate(['/books']);
        this.showInfo('Book updated', `${book.name} updated!`);
      },
      error => console.log(error)
    );
  }

  showInfo(title,message): void {
    swal.fire(title,message, 'success');
  }

}
