import { Component, OnInit } from '@angular/core';
import { Book } from './book';
import { BooksService } from './books.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  books : Book[];
  constructor(private bookService: BooksService) { }

  ngOnInit(): void {
    this.loadBooks();
  }

  loadBooks() {
    this.bookService.getBooks().subscribe(
      books =>  this.books = books
    );
  }

  deleteBook(id) {
    this.bookService.deleteBook(id).subscribe(
      () => {
        swal.fire('Book deleted', 'Book deleted successfully!', 'success');
        this.loadBooks();
      }
    );
  }

}
