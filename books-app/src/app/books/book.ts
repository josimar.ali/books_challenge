export class Book {
    id: number;
    name: string;
    author; string;
    genre: string;
    publisher: string;
    publishedYear: number;
}
