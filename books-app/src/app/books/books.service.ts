import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Book } from './book';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map} from 'rxjs/operators';
import swal from 'sweetalert2';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  private urlEndPoint : string = environment.bookApiUrl;
  private httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(private http: HttpClient) { }

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(this.urlEndPoint);
  }

  addBook(book: Book): Observable<Book> {
    return this.http.post<Book>(`${this.urlEndPoint}/add`, book, { headers: this.httpHeaders }).pipe(
      catchError (e =>  {
        this.showError(e.error);
        return throwError(e);
      })
    );
  }

  getBook(id): Observable<Book> {
    return this.http.get<Book>(`${this.urlEndPoint}/detail/${id}`).pipe(
      catchError (e =>  {
        this.showError(e.error);
        return throwError(e);
      })
    );
  }

  updateBook(book: Book): Observable<Book> {
    return this.http.put<Book>(`${this.urlEndPoint}/update/${book.id}`, book, {headers : this.httpHeaders}).pipe(
      catchError (e =>  {
        this.showError(e.error);
        return throwError(e);
      })
    );
  }

  deleteBook(id): Observable<Book> {
    return this.http.delete<Book>(`${this.urlEndPoint}/delete/${id}`).pipe(
      catchError (e => {
        this.showError(e.error);
        return throwError(e);
      })
    );
  }

  showError(error) {
    console.error(error.error);
    swal.fire('Internal Server Error',error.message, 'error');
  }
}
