export const environment = {
  production: true,
  bookApiUrl: 'https://springboot-service-books.herokuapp.com/api'
};
