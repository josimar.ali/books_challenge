package com.springboot.books.app.controllers;

import static org.junit.Assert.*;

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.springboot.books.app.models.entity.Book;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class BookControllerTest {

	private static final Logger LOG = Logger.getLogger(BookControllerTest.class.getName());

	@LocalServerPort
	private int port;

	TestRestTemplate restTemplate = new TestRestTemplate();

	@Test
	public void testFindById() {
		Book response = restTemplate.getForObject(createURLWithPort("/detail/1"), Book.class);
		assertNotNull(response);
	}
	
	@Test
	public void testNotFoundId() {
		Book response = restTemplate.getForObject(createURLWithPort("/detail/1333"), Book.class);
		assertNull(response.getId());
	}

	@Test
	public void testSave() {
		Book b = newBook("save_1", "author_1", null, null, null);
		HttpEntity<Book> entity = new HttpEntity<Book>(b);

		ResponseEntity<Book> response = restTemplate.exchange(createURLWithPort("/add"), HttpMethod.POST, entity,
				Book.class);
		assertEquals(HttpStatus.CREATED,response.getStatusCode());
	}

	@Test
	public void testDuplicateSave() {
		Book b = newBook("save_1_1", "author_1_1", null, null, null);
		HttpEntity<Book> entity = new HttpEntity<Book>(b);

		ResponseEntity<Book> response = restTemplate.exchange(createURLWithPort("/add"), HttpMethod.POST, entity,
				Book.class);

		b = newBook("save_1_1", "author_dulpicate_1_1", null, null, null);
		response = restTemplate.exchange(createURLWithPort("/add"), HttpMethod.POST, entity, Book.class);

		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	@Test
	public void testEmptyBookSave() {
		Book b = newBook(null, null, null, null, null);
		HttpEntity<Book> entity = new HttpEntity<Book>(b);

		ResponseEntity<Book> response = restTemplate.exchange(createURLWithPort("/add"), HttpMethod.POST, entity,
				Book.class);
		LOG.info(response.getStatusCode().toString());
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());		
	}

	@Test
	public void testBadYearSave() {
		Book b = newBook("save_badYear_1", "_save_author", null, null, 1000);
		HttpEntity<Book> entity = new HttpEntity<Book>(b);

		ResponseEntity<Book> response = restTemplate.exchange(createURLWithPort("/add"), HttpMethod.POST, entity,
				Book.class);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	public void testUpdate() {
		Book b = newBook("update_1", "author_1", null, null, null);
		HttpEntity<Book> entity = new HttpEntity<Book>(b);
		ResponseEntity<Book> response = restTemplate.exchange(createURLWithPort("/add"), HttpMethod.POST, entity,
				Book.class);
		if (response.getStatusCode() == HttpStatus.CREATED) {
			b = newBook("update_1_updated", "author_1_updated", "genre_updated", "publisher_updated", 2000);
			entity = new HttpEntity<Book>(b);
			Long id = response.getBody().getId();
			ResponseEntity<Book> updated = restTemplate.exchange(createURLWithPort("/update/" + id), HttpMethod.PUT,
					entity, Book.class);
			assertEquals(HttpStatus.CREATED, updated.getStatusCode());
		}
	}
	
	@Test
	public void testDuplicateUpdate() {
		Book b = newBook("update_2", "author_2", null, null, null);
		HttpEntity<Book> entity = new HttpEntity<Book>(b);
		ResponseEntity<Book> response = restTemplate.exchange(createURLWithPort("/add"), HttpMethod.POST, entity,
				Book.class);
		if (response.getStatusCode() == HttpStatus.CREATED) {
			b = newBook("The Lord of the Rings", "author_2_updated", "genre_updated", "publisher_updated", 2000);
			b.setName("The Lord of the Rings"); //already exists
			entity = new HttpEntity<Book>(b);
			Long id = response.getBody().getId();
			ResponseEntity<Book> updated = restTemplate.exchange(createURLWithPort("/update/" + id), HttpMethod.PUT,
					entity, Book.class);
			assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, updated.getStatusCode());
		}
	}

	@Test
	public void testEmptyNameUpdate() {
		Book b = newBook("update_3", "author_3", null, null, null);
		HttpEntity<Book> entity = new HttpEntity<Book>(b);
		ResponseEntity<Book> response = restTemplate.exchange(createURLWithPort("/add"), HttpMethod.POST, entity,
				Book.class);
		if (response.getStatusCode() == HttpStatus.CREATED) {
			b = newBook(null, "author_3_updated", "genre_updated", "publisher_updated", 2000);
			entity = new HttpEntity<Book>(b);
			Long id = response.getBody().getId();
			ResponseEntity<Book> updated = restTemplate.exchange(createURLWithPort("/update/" + id), HttpMethod.PUT,
					entity, Book.class);
			LOG.info(updated.getStatusCode().toString());
			assertEquals(HttpStatus.BAD_REQUEST, updated.getStatusCode());			
		}
	}

	@Test
	public void testNotFoundUpdate() {
		HttpEntity<Book> entity = new HttpEntity<Book>(newBook("update_4", "author_4_updated", "genre_updated", "publisher_updated", 2000));
		ResponseEntity<Book> updated = restTemplate.exchange(createURLWithPort("/update/1999"), HttpMethod.PUT, entity,
				Book.class);
		assertEquals(HttpStatus.NOT_FOUND, updated.getStatusCode());

	}

	private String createURLWithPort(String uri) {

		return "http://localhost:" + port + "/api" + uri;

	}

	private Book newBook(String name, String author, String genre, String publisher, Integer publishedYear) {
		name = name!=null?"BookTestControler_" + name:null;
		author = author!=null? "BookTestControler_" + author: null;
		genre = genre!=null? "BookTestControler_" + genre: null;
		publisher = publisher!=null? "BookTestControler_" + publisher: null;
		Book book = new Book(name, author);
		book.setGenre(genre);
		book.setpublisher(publisher);
		book.setPublishedYear(publishedYear);
		return book;
	}
}
