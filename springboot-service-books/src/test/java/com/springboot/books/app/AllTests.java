package com.springboot.books.app;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.springboot.books.app.controllers.BookControllerTest;
import com.springboot.books.app.models.dao.BookDaoTest;
import com.springboot.books.app.models.service.BookServiceTest;

@RunWith(Suite.class)
@SuiteClasses({
	BookDaoTest.class,
	BookControllerTest.class,
	BookServiceTest.class
})
public class AllTests {

}
