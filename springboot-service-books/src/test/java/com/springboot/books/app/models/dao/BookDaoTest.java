package com.springboot.books.app.models.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.springboot.books.app.models.entity.Book;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BookDaoTest {
	
	@Autowired
	private IBookDao dao;
	
	@Test
	public void addNewBook() {
		Book b = new Book();
		b.setName("TestBook1");
		b.setAuthor("TestAuthor1");
		Book result = dao.save(b);
		assertNotNull(result);
	}
	
	@Test(expected = org.springframework.dao.DataAccessException.class)
	public void addDulpicateBook() {
		Book b = new Book();
		b.setName("TestBook1");
		b.setAuthor("TestAuthor1");
		dao.save(b);
		b = new Book();
		b.setName("TestBook1");
		b.setAuthor("TestAuthor1");
		dao.save(b);
	}
	
	@Test(expected = javax.validation.ConstraintViolationException.class)
	public void addEmptyBook() {
		Book b = new Book();
		Book result = dao.save(b);
		assertNotNull(result);
	}
	
	@Test(expected = javax.validation.ConstraintViolationException.class)
	public void addWrongPublishedDateBook() {
		Book b = new Book();
		b.setName("TestBook1");
		b.setAuthor("TestAuthor1");
		b.setPublishedYear(1);
		Book result = dao.save(b);
		assertNotNull(result);
	}
	
	@Test
	public void getTestBook() {
		Book b = dao.findById((long) 1).orElse(null);
		assertNotNull(b);
	}
	
	@Test
	public void getNotFoundtBook() {
		Book b = dao.findById((long) 9865).orElse(null);
		assertNull(b);
	}
	
	@Test
	public void deleteBookById() {
		try {
			dao.deleteById((long)1);
		} catch (Exception e) {
			fail("book not deleted");
		}
		
	}
	
	@Test(expected = org.springframework.dao.DataAccessException.class)
	public void deleteMissingBookById() {
		dao.deleteById((long)5555);		
	}
}
