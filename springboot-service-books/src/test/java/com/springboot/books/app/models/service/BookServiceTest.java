package com.springboot.books.app.models.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.springboot.books.app.models.dao.IBookDao;
import com.springboot.books.app.models.entity.Book;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BookServiceTest {

    @Autowired
    private IBookService service;

    @MockBean
    private IBookDao dao;


	@Test
	public void testSave() {
		Book b  = newBook("save_1", "author_1", null, null, null);
		b.setId(21L);
		
		when(dao.save(b)).thenReturn(b);
		
		Book result = service.save(b);
		assertNotEquals(null, result.getId());
	}

	@Test
	public void testFindById() {
		Book b  = newBook("save_2", "author_2", null, null, null);
		b.setId(22L);
		
		when(dao.findById(22L)).thenReturn(Optional.of(b));
		
		Book result = service.findById(22L);
		System.out.println(result);
		assertNotEquals(null, result.getId());
	}
	
	@Test
	public void testNotFoundById() {
		when(dao.findById(23L)).thenReturn(Optional.of(new Book()));
		assertEquals(null, service.findById(23L).getId());
	}
	
	private Book newBook(String name, String author, String genre, String publisher, Integer publishedYear) {
		name = name!=null?"BookTestService_" + name:null;
		author = author!=null? "BookTestService_" + author: null;
		genre = genre!=null? "BookTestService_" + genre: null;
		publisher = publisher!=null? "BookTestService_" + publisher: null;
		Book book = new Book(name, author);
		book.setGenre(genre);
		book.setpublisher(publisher);
		book.setPublishedYear(publishedYear);
		return book;
	}

}
