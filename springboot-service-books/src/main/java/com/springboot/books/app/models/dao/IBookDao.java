package com.springboot.books.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.springboot.books.app.models.entity.Book;

public interface IBookDao extends CrudRepository<Book, Long>{

}
