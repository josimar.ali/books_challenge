package com.springboot.books.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootServiceBooksApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootServiceBooksApplication.class, args);
	}

}
