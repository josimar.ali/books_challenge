package com.springboot.books.app.models.service;

import java.util.List;

import com.springboot.books.app.models.entity.Book;

public interface IBookService {
	public List<Book> findAll();

	public Book save(Book book);
	
	public Book findById(Long id);
	
	public void delete(Long id);
}
