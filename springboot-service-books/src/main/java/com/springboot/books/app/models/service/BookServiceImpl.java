package com.springboot.books.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.books.app.models.dao.IBookDao;
import com.springboot.books.app.models.entity.Book;

@Service
public class BookServiceImpl implements IBookService{
	
	@Autowired
	private IBookDao dao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Book> findAll() {
		return (List<Book>) dao.findAll();
	}

	@Override
	public Book save(Book book) {
		return dao.save(book);
	}

	@Override
	@Transactional(readOnly = true)
	public Book findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	public void delete(Long id) {
		dao.deleteById(id);
	}

}
