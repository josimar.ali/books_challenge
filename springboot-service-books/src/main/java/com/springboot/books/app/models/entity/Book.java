package com.springboot.books.app.models.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "books")
public class Book implements Serializable {

	public Book() {}
	
	public Book(String name, String author) {
		this.name = name;
		this.author = author;
	}
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "it can't be empty")
	@Column(nullable = false, unique = true)
	private String name;

	@NotEmpty(message = "it can't be empty")
	private String author;
	private String genre;
	private String publisher;

	@Column(name = "published_year")
	@Max(value = 2155, message = "must be less or equal to 2155")
	@Min(value = 1800, message = "must be greater or equal to 1800")
	private Integer publishedYear;

	public Integer getPublishedYear() {
		return publishedYear;
	}

	public void setPublishedYear(Integer publishedYear) {
		this.publishedYear = publishedYear;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getpublisher() {
		return publisher;
	}

	public void setpublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	@Override
	public String toString() {
		return "{ " + "id: " + id + ", name: " + name + ", author: " + author + ", genre: " + genre + ", publisher: "
				+ publisher + ", publishedYear: " + publishedYear + " }";
	}

	private static final long serialVersionUID = -4129371466426466739L;

}
