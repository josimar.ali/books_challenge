package com.springboot.books.app.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.books.app.models.entity.Book;
import com.springboot.books.app.models.service.IBookService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class BookController {
	
	@Autowired
	private IBookService service;
	
	@GetMapping({"","/","/list"})
	public List<Book> list(){
		return service.findAll();
	}
	
	@GetMapping("/detail/{id}")
	public ResponseEntity<?> findById(@PathVariable Long id) {
		Book b = null;
		Map<String, Object> response = new HashMap<String,Object>();
		try {
			b = service.findById(id);
			if(b == null) {
				response.put("message", "Book not found");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
			}
		} catch(DataAccessException ex) {
			response.put("message", "Book not found");
			response.put("error",ex.getMessage() + ": " + ex.getMostSpecificCause());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Book>(b, HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> save(@Valid @RequestBody Book book, BindingResult result) {
		Book b = null;
		Map<String, Object> response = new HashMap<String,Object>();
		if(result.hasErrors()) {
			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "Field " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);
			response.put("message", "Error while adding new book");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			b = service.save(book);
		} catch (DataAccessException ex) {
			response.put("message", "Error while adding new book");
			response.put("error",ex.getMessage() + ": " + ex.getMostSpecificCause());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (ConstraintViolationException ex) {
			response.put("message", "Error while adding new book. Book name already exists");
			response.put("error",ex.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception ex) {
			response.put("message", "Error while adding new book");
			response.put("error",ex.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Book>(b, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<String,Object>();
		try {
			service.delete(id);
		} catch(DataAccessException ex) {
			response.put("message", "Error while deleting book");
			response.put("error",ex.getMessage() + ": " + ex.getMostSpecificCause());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("message", "Book deleted");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Book book, BindingResult result, @PathVariable Long id) {
		Book b = null;
		Book updated = null;
		Map<String, Object> response = new HashMap<String,Object>();
		if(result.hasErrors()) {
			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "Field " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);
			response.put("message", "Error while editing book data");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			b = service.findById(id);
			if(b == null) {
				response.put("message", "Book not found");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
			} else {
				b.setName(book.getName());
				b.setAuthor(book.getAuthor());
				b.setGenre(book.getGenre());
				b.setpublisher(book.getpublisher());
				b.setPublishedYear(book.getPublishedYear());
			}
			updated = service.save(b);	
		} catch (DataAccessException ex) {
			response.put("message", "Error while editing book data");
			response.put("error",ex.getMessage() + ": " + ex.getMostSpecificCause());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (ConstraintViolationException ex) {
			response.put("message", "Error while editing book data. Book name already exists");
			response.put("error",ex.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception ex) {
			response.put("message", "Error while editing book data");
			response.put("error",ex.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Book>(updated, HttpStatus.CREATED);
	}

}
